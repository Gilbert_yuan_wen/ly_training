# -*- coding: utf-8-*- 
from openerp.osv import osv
from openerp.osv import fields
class training_student(osv.osv):
	_name='training.student'
	_rec_name='name'
	_columns={
	'name':fields.char(u'学生姓名',size=64),
	'email':fields.char(u'电子邮箱',size=64),
	}
	def join_lesson(self,cr,uid,ids,context=None):
		lesson=self.pool.get('training.lesson')
		for apply in self.browse(cr,uid,ids,context=context):
			lesson.write(cr,uid,apply.mylesson.id,{'student':[(4,apply.name.id)]},context)
		return True