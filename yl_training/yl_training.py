# -*- coding: utf-8-*- 
from openerp.osv import osv
from openerp.osv import fields
from  datetime  import  datetime  
import re
import time,datetime
class training_subject(osv.osv):
	_name='training.subject'
	_rec_name='name'
	_columns={
	'name':fields.char(u'名称',size=64),
	'describle':fields.char(u'描述',size=64),
	'people':fields.many2one('res.users','管理员'),
	'lessonmessage':fields.one2many('training.lesson','subject','课程信息'),#many2one关系设置
    }
	_defaults={
	'name': u'科目名称',
	'describle': u'描述1',
	'people':u'管理员',
	}
	def name_get(self,cr,uid,ids,context=None):  
	#lessonmessage 计算lesson中使用lessonmessage 的次数在 lesson 选用subject时显示这个次数
		res=super(training_subject,self).name_get(cr,uid,ids,context=context)
		for index,name in enumerate(res):
			subject=self.browse(cr,uid,name[0],context=context)
			if subject.lessonmessage:
				length=len([line for line in subject.lessonmessage])
				res[index]=(name[0],subject.name+' '+str(length))
		return res

class training_lesson(osv.osv):
	_name='training.lesson'
	_rec_name='name'
	def _get_continue_days(self,cr,uid,ids,name,arg,context=None):
		res = {}
		print 'I,nnjub' 
		for lesson in self.browse(cr,uid,ids,context=context):
			if lesson.begintime and lesson.endtime:
				begintime = datetime.datetime.strptime(lesson.begintime,'%Y-%m-%d') # datetime用这个%Y-%m-%d %H:%M:%S
				endtime = datetime.datetime.strptime(lesson.endtime,'%Y-%m-%d')
				res.update({lesson.id:(endtime-begintime).days})
				
		return res
	def _get_join_number(self,cr,uid,ids,name,arg,context=None):
		res = {}
		for lesson in self.browse(cr,uid,ids,context=context):
			if lesson.student:
				length=len([line for line in lesson.student])
				res.update({lesson.id:1.0*length/lesson.seat*100})
			return res
	def _get_leftseat(self,cr,uid,ids,name,arg,context=None):
		res = {}
		for lesson in self.browse(cr,uid,ids,context=context):

			if lesson.student:
				length=len([line for line in lesson.student])
				res.update({lesson.id:lesson.seat-length})
			else:
				res.update({lesson.id:lesson.seat})

			return res	
  
	_columns={
	'name':fields.char(u'课程名',size=64),
	'describle':fields.char(u'描述',size=64),
	'subject':fields.many2one('training.subject','科目'),
	'student':fields.many2many('res.partner','student_lesson','lesson_id','student_id','学生'),
	'teacher':fields.many2one('res.partner','老师'),#,domain="[('is_teacher','=',True)]"
	'seat':fields.integer(u'座位数',size=64),
	'begintime':fields.date(u'开始时间'),
	'endtime':fields.date(u'结束时间'),
	'state':fields.selection([('0','草稿'),('1','正常'),('2','一次审批'),('3','二次审批'),('4','时间已过'),('5','学生数已满'),('6','准备上课'),('7','课程结束')],u'审批状态' ),
	'allday':fields.function(_get_continue_days,type='integer',string='持续时间',store=True),
	'joinpeople':fields.function(_get_join_number,type='float',string='报名进度',store=True),
	'leftseat':fields.function(_get_leftseat,type='integer',string='剩余席位',store=False),
	 
	}
	def	approved1(self,cr,uid,ids,context=None):
		lesson_obj=self.pool.get('training.lesson')
		for lesson in self.browse(cr,uid,ids,context=context):
			if lesson.state==2:
				lesson_obj.write(cr,uid,ids,{'state':'1'},context=context)
			if lesson.state==1:
				lesson_obj.write(cr,uid,ids,{'state':'3'},context=context)

	_defaults={
	'name': u'课程1',
	'describle': u'描述1',
	'seat':u'0',
 
	}
 
	def check_date(self,cr,uid,ids,context=None):
		for lesson in self.browse(cr,uid,ids,context=context):
			if lesson.begintime >= lesson.endtime:
				return False
					
		return True
	_sql_constraints=[('uniq_name','unique(name)','课程名必须唯一'),]
	def checkzero(self,cr,uid,ids,context=None):
		for lesson in self.browse(cr,uid,ids,context=context):
			if lesson.allday<=0:
				return False
		return True
	def checkzero1(self,cr,uid,ids,context=None):
		for lesson in self.browse(cr,uid,ids,context=context):
			if lesson.seat<=0:
				return False
		return True

	_constraints=[(checkzero1,u'座位数必须大于零',['seat']),
	(check_date,u'结束时间必须大于开始时间',['begintime','endtime']),
	(checkzero,u'持续日期必须大于零',['allday'])]

class training_join(osv.osv_memory):
	_name='training.join'
	_rec_name='name'
	_columns={
	'name':fields.many2one('res.partner','报名学生信息'),
	'mylesson':fields.many2one('training.lesson','课程信息'),
	}
	def join_lesson(self,cr,uid,ids,context=None):
		lesson=self.pool.get('training.lesson')
		for apply in self.browse(cr,uid,ids,context=context):
			lesson.write(cr,uid,apply.mylesson.id,{'student':[(4,apply.name.id)]},context)
		return True

class training_teaher(osv.osv):
	_name='training.teacher'
	_rec_name='name'
	_columns={
	'name':fields.char(u'课程名',size=64),
	'describle':fields.char(u'描述',size=64),
	}

class res_partner(osv.osv):
	_inherit='res.partner'

	_columns={
	    'is_teacher':fields.boolean(u'老师'),    
	}